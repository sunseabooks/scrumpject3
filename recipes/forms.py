from django import forms
from .models import Recipe, Ingredient, Step

class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "name",
            "description",
            "image",
            "tag",
        ]

class IngredientForm(forms.ModelForm):
    class Meta:
        model = Ingredient
        fields = [
            "amount",
            "measure",
            "food",
        ]

class StepForm(forms.ModelForm):
    class Meta:
        model = Step
        fields = [
            "order",
            "directions",
            "ingredient",
            "image",
        ]

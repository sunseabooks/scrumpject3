from django.db import models
from django.core.validators import MaxValueValidator
from django.conf import settings
from .manager import ScrumpManager

USER_MODEL = settings.AUTH_USER_MODEL


class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.ForeignKey(
        USER_MODEL, related_name="recipes", on_delete=models.SET_NULL, null=True
    )
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    tag = models.ManyToManyField(
        "tags.Tag", related_name="recipes"
    )
    # objects = ScrumpManager()

    def __str__(self):
        return f"{self.name} by {self.author}"

class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return f"{self.name}"

class MeasureEqual(models.Model):
    home = models.ForeignKey(
        "Measure", related_name="homes", on_delete=models.CASCADE
    )
    away = models.ForeignKey(
        "Measure", related_name="aways", on_delete=models.CASCADE
    )
    ratio = models.FloatField()

    def __str__(self):
        return f"{self.home} = {self.ratio} {self.away}"

class Ingredient(models.Model):
    amount = models.FloatField(validators=[MaxValueValidator(1000)])
    recipe = models.ForeignKey(
        "Recipe", related_name="ingredients", on_delete=models.CASCADE
    )
    measure = models.ForeignKey(
        "Measure", related_name="ingredients", on_delete=models.CASCADE
    )
    food = models.CharField(max_length=25)

    def __str__(self):
        return f"{self.amount} {self.measure} {self.food}"

class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe", related_name="steps", on_delete=models.CASCADE
    )
    order = models.PositiveSmallIntegerField()
    image = models.URLField(null=True, blank=True)
    directions = models.CharField(max_length=300)
    ingredient = models.ForeignKey(
        "Ingredient", related_name="steps", on_delete=models.CASCADE
    )

    def __str__(self):
        return f"Step {self.order}: {self.directions} {self.ingredient}"

from django.urls import path
from .views import recipeCreate, ingredientCreate, stepCreate, recipeUpdate, ingredientUpdate, stepUpdate, recipeDetail, recipeList, stepDelete, ingredientDelete, searchOut, frontPage, RecipeDelete

urlpatterns = [
    path("list/", recipeList, name="recipe_list"),
    path("create/", recipeCreate, name="recipe_create"),
    path("<int:recipe_id>/update/", recipeUpdate, name="recipe_update"),
    path("<int:recipe_id>/detail/", recipeDetail, name="recipe_detail"),
    path("<int:recipe_id>/delete/", RecipeDelete.as_view(), name="recipe_delete"),

    path("ingredients/<int:recipe_id>/create", ingredientCreate, name="ingredient_create"),
    path("ingredients/<int:ingredient_id>/update", ingredientUpdate, name="ingredient_update"),
    path("ingredients/<int:ingredient_id>/delete", ingredientDelete, name="ingredient_delete"),

    path("steps/<int:ingredient_id>/create/", stepCreate, name="step_create"),
    path("steps/<int:step_id>/update/", stepUpdate, name="step_update"),
    path("steps/<int:step_id>/delete/", stepDelete, name="step_delete"),

    path("search/", searchOut, name="search_out"),

    path("front/", frontPage, name="front"),
]

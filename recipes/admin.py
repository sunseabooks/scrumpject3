from django.contrib import admin
from .models import Recipe, Measure, MeasureEqual, Ingredient, Step
admin.site.register(Recipe)
admin.site.register(Measure)
admin.site.register(MeasureEqual)
admin.site.register(Ingredient)
admin.site.register(Step)

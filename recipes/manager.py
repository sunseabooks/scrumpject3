from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

class ScrumpQuerySet(models.QuerySet):

    def filter_list_byAuthor(self, authorname):
        return self.filter(author__username__icontains=authorname)

    def filter_list_byTitle(self, title):
        return self.filter(name__icontains=title)

    def get_latest_item(self, modelfield):
        return self.latest(modelfield)

    def get_object_item(self, pk):
        return self.filter(id=pk)

    # def get_foreign_object(self, object):
    #     return self.filter(recipes__mealplans__)


class ScrumpManager(models.Manager):
    def get_queryset(self):
        return ScrumpQuerySet(self.model, using=self._db)

    def filter_byAuthor(self, authorname):
        return self.get_queryset().filter_list_byAuthor(authorname)

    def filter_byTitle(self, title):
        return self.get_queryset().filter_list_byTitle(title)

    def get_latest(self, modelfield):
        return self.get_queryset().get_latest_item(modelfield)

    def get_object(self, pk):
        return self.get_queryset().get_object_item(pk)

    def get_foreign(self, object):
        return self.get_queryset().get_foreign_object(object)

def filterRef(filter_inRequest, search_inRequest, filter_param, search_param):
    context_list={}
    for filter in filter_param:
        context_variable = filter_param.get(filter)[1]
        model = filter_param.get(filter)[2]
        context_name = filter_param.get(filter)[0]
        if filter_inRequest == {}:
            context_variable = contextRef(search_inRequest, search_param, model)
            context_list[context_name]= context_variable
        else:
            if filter in filter_inRequest:
                context_variable = contextRef(search_inRequest, search_param, model)
                context_list[context_name]= context_variable
            else:
                context_list[context_name]=""
    return context_list


def contentParse(request, some_param):
    parseRef = {}
    for param in some_param:
        if request.get(param) != None and request.get(param) != "":
            parseRef[param] = request[param]
    return parseRef

def contextRef(search_inRequest, search_param, Model):
    context_objects = Model.objects
    for key in search_inRequest:
        if key in search_param:
            query_param = search_param.get(key)
            context_objects = context_objects.filter(**query_param)
    return context_objects

def contextRef(search_inRequest, search_param, Model):
    context_objects = Model.objects
    for key in search_inRequest:
        if key in search_param:
            query_param = search_param.get(key)
            context_objects = context_objects.filter(**query_param)
    return context_objects

    #This function updates the context_object through the query conditions assigned to the key
    #   the query conditions is the value of search_param
    #       updates the context_object
    #   function repeats refining for all keys found in search_inRequest#

def filterRef(filter_inRequest, search_inRequest, filter_param):
    context_name = filter_param.get(filter)[0]
    context_variable = filter_param.get(filter)[1]
    model = filter_param.get(filter)[2]
    context_list={}
    for filter in filter_param:
        if filter_inRequest == {}:
            context_variable = contextRef(search_inRequest, model)
            context_list[context_name]= context_variable
        else:
            if filter in filter_inRequest:
                context_variable = contextRef(search_inRequest, model)
                context_list[context_name]= context_variable
            else:
                context_list[context_name]=""
    return context_list
    #this updates the context variable defined below the searchOut function name,
    #   by using contentParse to produce a object instance
    #       using the filters_inRequest and the Model name of the corresponding filter #

def contentParse(request, some_param):
    parseRef = {}
    for param in some_param:
        if request.GET.get(param) != None and request.GET.get(param) != "":
            parseRef[param] = request.GET[param]
    return parseRef
    #This function checks which filter in filters are present in the request
    #If present the function grabs the corresponding input value from the request
    # pareseRef is a dictionary of {filter present in request: value of filter in request}#

def searchOut(request):
    recipes = ""
    mealdays = ""
    mealweeks = ""
    # insert/delete above to change variables used for render(context) #
    query_title = dict(name__icontains=request.GET.get("search_title"))
    query_author = dict(author__username__icontains=request.GET.get("search_author"))
    #create a query_param for each search box on page #
    search_param = {
        "search_title": query_title,
        "search_author":query_author,
        }
    # correspond button/input name to query_param #
    filter_param = {
        "check_recipe":["recipes",recipes, Recipe],
        "check_mealdays":["mealdays", mealdays, MealDay],
         "check_mealweek":["mealweeks", mealweeks, MealWeek]
         }
    #for "check_recipe":["recipes",recipes, Recipe]
    #   check_recipe is the input name,
    #   check_recipe[0] is the string name for context passed into render
    #   check_recipe[1] is the variable name for context passed into render
    #   check_recipe[2] is the model for the corresponding objects

    search_inRequest = contentParse(request, search_param)
    filter_inRequest = contentParse(request, filter_param)
    context = filterRef(filter_inRequest, search_inRequest, filter_param, search_param)
    #   filterRef uses filter_param, to derive, check_recipe[0], check_recipe[1], check_recipe[2]
    #   filterRef obtains object, check_recipe[1], using model, check_recipe[2]
    #       creates a context dictionary entry in the form of {"check_recipe[0]":check_recipe[1]}

    return render(request, "search/out.html", context)

from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import DeleteView
from .models import Recipe, Ingredient, Step
from mealplans.models import MealWeek, MealDay, MealTime
from .forms import RecipeForm, IngredientForm, StepForm
from .search_function import contentParse, filterRef
from django import forms

@login_required
def recipeList(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {"recipes": recipes}
    return render(request, "recipes/list.html", context)

@login_required
def recipeCreate(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(commit=False)
            recipe.author = request.user
            recipe.save()
            form.save_m2m()
            return redirect("ingredient_create", recipe.pk)
    else:
        form = RecipeForm()
    context = {"form":form}
    return render(request, "recipes/create.html", context)

@login_required
def ingredientCreate(request, recipe_id):
    if request.method == "POST":
        form = IngredientForm(request.POST)
        if form.is_valid():
            ingredient = form.save(commit=False)
            ingredient.recipe = Recipe.objects.get(pk=recipe_id)
            ingredient.save()
            return redirect("step_create", ingredient.pk)
    else:
        form = IngredientForm()
    context = {"form": form}
    return render(request, "ingredients/create.html", context)

@login_required
def stepCreate(request, ingredient_id):
    if request.method == "POST":
        form = StepForm(request.POST)
        form.fields["ingredient"]=forms.ModelChoiceField(
            queryset=Ingredient.objects.filter(author=request.user)
        )
        if form.is_valid():
            step = form.save(commit=False)
            ingredient = Ingredient.objects.get(pk=ingredient_id)
            step.ingredient = ingredient
            step.recipe = ingredient.recipe
            step.save()
            return redirect("recipe_detail", ingredient.recipe.id)
    else:
        form = StepForm()
    context = {"form": form}
    return render(request, "steps/create.html", context)


def recipeDetail(request, recipe_id):
    recipe = Recipe.objects.get(pk=recipe_id)
    ingredients = Ingredient.objects.filter(recipe=recipe_id)
    steps_recipe = Step.objects.filter(recipe=recipe_id)
    context = {
        "recipe": recipe,
        "ingredients": ingredients,
        "steps_recipe": steps_recipe,
    }
    return render(request, "recipes/detail.html", context)

@login_required
def recipeUpdate(request, recipe_id):
    old = Recipe.objects.get(pk=recipe_id)
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            new = form.save(commit=False)
            new.save()
            form.save_m2m()
            return redirect("recipe_detail", recipe_id)
    else:
        form = RecipeForm(instance=old)
    context = {"form": form}
    return render(request, "recipes/update.html", context)

@login_required
def ingredientUpdate(request, recipe_id):
    pk = request.GET.get("ingredient_update")
    old = Ingredient.objects.get(pk=pk)
    if request.method == "POST":
        form = IngredientForm(request.POST, instance=old)
        if form.is_valid():
            new = form.save(commit=False)
            new.save()
            form.save_m2m()
            return redirect("ingredient_detail",recipe_id)
    else:
        form = IngredientForm(instance=old)
    context = {"form": form}
    return render(request, "ingredients/update.html", context)

@login_required
def stepUpdate(request, recipe_id):
    pk = request.GET.get("step_update")
    old = Step.objects.get(pk=pk)
    if request.method == "POST":
        form = StepForm(request.POST, instance=old)
        if form.is_valid():
            new = form.save(commit=False)
            new.save()
            form.save_m2m()
            return redirect("recipe_detail", recipe_id)
    else:
        form = StepForm(instance=old)
    context = {"form": form}
    return render(request, "steps/update.html", context)

@login_required
def ingredientDelete(request, recipe_id):
    if request.method == "POST":
        pk = request.POST.get("ingredient_delete")
        ingredient = Ingredient.objects.get(pk=pk)
        ingredient.delete()
        return redirect("recipe_detail", recipe_id)

@login_required
def stepDelete(request, recipe_id):
    if request.method == "POST":
        pk = request.POST.get("step_delete")
        step = Step.objects.get(pk=pk)
        step.delete()
        return redirect("recipe_detail", recipe_id)

def searchOut(request):
    recipes = ""
    mealtimes = ""
    mealdays = ""
    mealweeks = ""
    query_title = dict(name__icontains=request.GET.get("search_title"))
    query_author = dict(author__username__icontains=request.GET.get("search_author"))
    search_param = {
        "search_title": query_title,
        "search_author": query_author,
        }
    filter_param = {
        "check_recipes":["recipes", recipes, Recipe],
        "check_mealtimes":["mealtimes", mealtimes, MealTime],
        "check_mealdays":["mealdays", mealdays, MealDay],
         "check_mealweeks":["mealweeks", mealweeks, MealWeek],
         }
    search_inRequest = contentParse(request.GET, search_param)
    filter_inRequest = contentParse(request.GET, filter_param)
    context = filterRef(filter_inRequest, search_inRequest, filter_param, search_param)

    return render(request, "search/out.html", context)

def frontPage(request):
    frontQuery = {
        "recipe_created": Recipe,
        "recipe_updated": Recipe,
        "mealtime": MealTime,
        "mealday": MealDay,
        "mealweek": MealWeek,
    }

    dict = {}
    for variable in frontQuery:
        model = frontQuery.get(variable)
        try:
            dict[variable] = model.objects.latest("created")
        except model.DoesNotExist:
            dict[variable] = ""

    context = {
        "recipe_created": dict.get("recipe_created"),
        "recipe_updated": dict.get("recipe_updated"),
        "mealtime": dict.get("mealtime"),
        "mealday": dict.get("mealday"),
        "mealweek": dict.get("mealweek"),
    }
    return render(request, "front.html", context)

class RecipeDelete(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name="recipes/delete.html"
    success_url = reverse_lazy("recipe_list")

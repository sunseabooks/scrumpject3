from django.shortcuts import render, redirect
from .forms import TagForm

def tagCreate(request):
    if request.method == "POST":
        form = TagForm(request.POST)
        if form.is_valid():
            tag = form.save(commit=False)
            tag.save()
            form.save_m2m
            return redirect("home")
    else:
        form = TagForm()
    context = {"form": form}
    return render(request, "tags/create.html", context)

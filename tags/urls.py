from django.urls import path
from .views import tagCreate

urlpatterns = [
    path("", tagCreate, name="tag_create")
]

from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from accounts.models import FavRecipe, FavMealTime
from recipes.models import Recipe
from mealplans.models import MealTime, MealDay, MealWeek
from django.views.generic.edit import DeleteView
from .forms import MealTimeForm, MealDayForm, MealWeekForm
from django import forms

@login_required
def mealTimeCreate(request):
    form = MealTimeForm()
    form.fields["my_recipe"]=forms.ModelMultipleChoiceField(
        queryset=Recipe.objects.filter(author=request.user),
        required=False
    )
    form.fields["fav_recipe"]=forms.ModelMultipleChoiceField(
        queryset=FavRecipe.objects.filter(author=request.user),
        required=False
    )
    if request.method == "POST":
        form = MealTimeForm(request.POST)
        if form.is_valid():
            mealtime = form.save(commit=False)
            mealtime.author = request.user
            mealtime.save()
            form.save_m2m()
            return redirect("mealtime_detail", mealtime.id)
    context = {"form":form}
    return render(request, "mealtimes/create.html", context)

@login_required
def mealDayCreate(request):
    form = MealDayForm()
    form.fields["my_mealtime"]=forms.ModelMultipleChoiceField(
        queryset=MealTime.objects.filter(author=request.user),
        required=False
    )
    form.fields["fav_mealtime"]=forms.ModelMultipleChoiceField(
        queryset=FavMealTime.objects.filter(author=request.user),
        required=False
    )
    if request.method == "POST":
        form = MealDayForm(request.POST)
        if form.is_valid():
            mealday = form.save(commit=False)
            mealday.author = request.user
            mealday.save()
            form.save_m2m()
            return redirect("mealday_detail", mealday.pk)
    context = {"form": form}
    return render(request, "mealdays/create.html", context)

@login_required
def mealWeekCreate(request):
    if request.method == "POST":
        form = MealWeekForm(request.POST)
        if form.is_valid():
            mealweek = form.save(commit=False)
            mealweek.author = request.user
            form.save_m2m()
            return redirect("mealweek_detail", mealweek.pk)
    else:
        form = MealWeekForm()
    context={"form":form}
    return render(request, "mealweeks/create.html", context)


@login_required
def mealTimeUpdate(request, mealtime_id):
    old = MealTime.objects.get(pk=mealtime_id)
    form = MealTimeForm()
    form.fields["recipe"]=forms.ModelMultipleChoiceField(
        queryset=FavRecipe.objects.filter(author=request.user),
        required=False
    )
    if request.method == "POST":
        form = MealTimeForm(request.POST, instance=old)
        if form.is_valid():
            mealtime = form.save(commit=False)
            mealtime.save()
            form.save_m2m()
            return redirect("mealtime_detail", mealtime_id)
    else:
        form = MealTimeForm(instance=old)
    context = {"form": form}
    return render(request, "mealtimes/update.html", context)

@login_required
def mealDayUpdate(request, mealday_id):
    old = MealDay.objects.get(pk=mealday_id)
    form = MealDayForm()
    form.fields["mealtime"]=forms.ModelMultipleChoiceField(
        queryset=MealTime.objects.filter(author=request.user),
        required=False
    )
    if request.method == "POST":
        form = MealDayForm(request.POST, instance=old)
        if form.is_valid():
            mealday = form.save(commit=False)
            mealday.save()
            form.save_m2m()
            return redirect("mealday_detail", mealday_id)
    else:
        form = MealDayForm(instance=old)
    context = {"form": form}
    return render(request, "mealdays/update.html", context)

@login_required
def mealWeekUpdate(request, mealweek_id):
    old = MealWeek.objects.get(pk=mealweek_id)
    if request.method == "POST":
        form = MealWeekForm(request.POST, instance=old)
        if form.is_valid():
            mealweek = form.save(commit=False)
            mealweek.save()
            form.save_m2m()
            return redirect("mealweek_detail", mealweek_id)
    else:
        form = MealWeekForm(isntance=old)
    context = {"form": form}
    return render(request, "mealweeks/update.html", context)

@login_required
def mealTimeList(request):
    mealtimes = MealTime.objects.filter(author=request.user)
    context = {"mealtimes": mealtimes}
    return render(request, "mealtimes/list.html", context)

@login_required
def mealDayList(request):
    mealdays = MealDay.objects.filter(author=request.user)
    context = {"mealdays": mealdays}
    return render(request, "mealdays/list.html", context)

@login_required
def mealWeekList(request):
    mealweeks = MealWeek.objects.filter(author=request.user)
    context = {"mealweeks": mealweeks}
    return render(request, "mealweeks/list.html", context)


def mealTimeDetail(request, mealtime_id):
    mealtime = MealTime.objects.get(pk=mealtime_id)
    context = {"mealtime": mealtime}
    return render(request, "mealtimes/detail.html", context)


def mealDayDetail(request, mealday_id):
    mealday = MealDay.objects.get(pk=mealday_id)
    context = {"mealday": mealday}
    return render(request, "mealdays/detail.html", context)

def mealWeekDetail(request, mealweek_id):
    mealweek = MealWeek.objects.get(pk=mealweek_id)
    context = {"mealweek": mealweek}
    return render(request, "mealweeks/detail.html", context)


class MealTimeDelete(DeleteView):
    model = MealTime
    template_name = "mealtimes/delete.html"
    success_url = reverse_lazy("mealtime_list")

class MealDayDelete(DeleteView):
    model = MealDay
    template_name = "mealdays/delete.html"
    success_url = reverse_lazy("mealday_list")

class MealWeekDelete(DeleteView):
    model = MealWeek
    template_name = "mealweeks/delete.html"
    success_url = reverse_lazy("mealweek_list")

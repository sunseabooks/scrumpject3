from django.urls import path
from .views import mealTimeCreate, mealDayCreate, mealWeekCreate, mealTimeUpdate, mealDayUpdate, mealWeekUpdate, mealTimeList, mealDayList, mealWeekList, mealTimeDetail, mealDayDetail, mealWeekDetail, MealTimeDelete, MealDayDelete, MealWeekDelete

urlpatterns = [
    path("mealtimes/create", mealTimeCreate, name="mealtime_create"),
    path("mealtimes/<int:mealtime_id>/detail", mealTimeDetail, name="mealtime_detail"),
    path("mealtimes/<int:mealtime_id>/update", mealTimeUpdate, name="mealtime_update"),
    path("mealtimes/<int:mealtime_id>/delete", MealTimeDelete.as_view(), name="mealtime_delete"),
    path("mealtimes/list", mealTimeList, name="mealtime_list"),

    path("mealdays/create", mealDayCreate, name="mealday_create"),
    path("mealdays/<int:mealday_id>/detail", mealDayDetail, name="mealday_detail"),
    path("mealdays/<int:mealday_id>/update", mealDayUpdate, name="mealday_update"),
    path("mealdays/<int:mealday_id>/delete", MealDayDelete.as_view(), name="mealday_delete"),
    path("mealdays/list", mealDayList, name="mealday_list"),


    path("mealweeks/create", mealWeekCreate, name="mealweek_create"),
    path("mealweeks/<int:mealweek_id>/detail", mealWeekDetail, name="mealweek_detail"),
    path("mealweeks/<int:mealweek_id>/update", mealWeekUpdate, name="mealweek_update"),
    path("mealweeks/<int:mealweek_id>/delete", MealWeekDelete.as_view(), name="mealweek_delete"),
    path("mealweeks/list", mealWeekList, name="mealweek_list"),
]

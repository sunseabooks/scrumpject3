from django.db import models
from django.conf import settings


USER_MODEL =  settings.AUTH_USER_MODEL

class MealTime(models.Model):
    name = models.CharField(max_length=25)
    created = models.DateTimeField(auto_now_add=True)
    time = models.TimeField()
    description = models.CharField(max_length=300, null=True, blank=True)
    author = models.ForeignKey(
        USER_MODEL, related_name="mealtimes", on_delete=models.SET_NULL, null=True
    )
    fav_recipe = models.ManyToManyField(
        "accounts.FavRecipe", related_name="mealtimes", blank=True
        )
    my_recipe = models.ManyToManyField(
        "recipes.Recipe", related_name="mealtimes", blank=True
    )
    def __str__(self):
        return f"{self.name}"

class MealDay(models.Model):
    #there is a foreignkey to FavMealDay in account model
    name = models.CharField(max_length=25)
    created = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=300, null=True, blank=True)
    author = models.ForeignKey(
        USER_MODEL, related_name="mealdays", on_delete=models.SET_NULL, null=True
    )
    my_mealtime = models.ManyToManyField(
        "MealTime", related_name="mealdays", blank=True
    )
    fav_mealtime = models.ManyToManyField(
        "accounts.FavMealTime", related_name="mealdays", blank=True
    )

    # objects = ScrumpManager()
    def __str__(self):
        return f"{self.name}"

class MealWeek(models.Model):
    #there is a foreignkey to FavMealWeekin account model
    name = models.CharField(max_length=25)
    description = models.CharField(max_length=350)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(
        USER_MODEL, related_name="mealweeks", on_delete=models.SET_NULL, null=True
    )
    monday = models.ForeignKey(
        "MealDay", related_name="mealweeks_mondays", on_delete=models.SET_NULL, null=True, blank=True
    )
    tuesday = models.ForeignKey(
        "MealDay", related_name="mealweeks_tuesdays", on_delete=models.SET_NULL, null=True, blank=True
    )
    wednesday = models.ForeignKey(
        "MealDay", related_name="mealweeks_wednesdays", on_delete=models.SET_NULL, null=True, blank=True
    )
    thursday = models.ForeignKey(
        "MealDay", related_name="mealweeks_thursdays", on_delete=models.SET_NULL, null=True, blank=True
    )
    friday = models.ForeignKey(
        "MealDay", related_name="mealweeks_fridays", on_delete=models.SET_NULL, null=True, blank=True
    )
    saturday = models.ForeignKey(
        "MealDay", related_name="mealweeks_saturdays", on_delete=models.SET_NULL, null=True, blank=True
    )
    sunday = models.ForeignKey(
        "MealDay", related_name="mealweeks_sundays", on_delete=models.SET_NULL, null=True, blank=True
    )
    # objects = ScrumpManager()
    def __str__(self):
        return f"{self.name} by {self.author}"

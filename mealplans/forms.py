from django import forms
from accounts.models import FavRecipe
from .models import MealTime, MealDay, MealWeek

# baseList = [
#     "review",
#     "rating",
#     ]
# mealDayList = [

# ]

# def fieldsCompiler(request, baseList, extraDict):
#     fieldsList = baseList
#     for flag in extraDict:
#         if flag in request.path:
#             print("JOSSSSSSSSSSSIAH FLAG", flag)
#             field = extraDict.get(flag)
#             print("FIELD", field)
#             fieldsList.append(field)
#     return fieldsList

class MealTimeForm(forms.ModelForm):
    class Meta:
        model = MealTime
        fields=[
            "name",
            "time",
            "description",
            "my_recipe",
            "fav_recipe",
        ]

class MealDayForm(forms.ModelForm):
    class Meta:
        model = MealDay
        fields = [
            "name",
            "description",
            "my_mealtime",
            "fav_mealtime",
            ]

class MealWeekForm(forms.ModelForm):
    class Meta:
        model = MealWeek
        fields = [
            "name",
            "description",
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday",
            "sunday",
        ]

from django.contrib import admin
from .models import MealTime, MealDay, MealWeek

admin.site.register(MealTime)
admin.site.register(MealDay)
admin.site.register(MealWeek)

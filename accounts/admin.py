from django.contrib import admin
from .models import FavRecipe, FavMealTime, FavMealDay, FavMealWeek, ShoppingList, Review

admin.site.register(FavRecipe)
admin.site.register(FavMealTime)
admin.site.register(FavMealWeek)
admin.site.register(FavMealDay)
admin.site.register(ShoppingList)
admin.site.register(Review)

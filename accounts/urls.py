from django.urls import path
from django.contrib.auth import views as auth_views
from .views import signup, profileDetail, reviewCreate, reviewUpdate, reviewList, shoppingListCreate, shoppingListDetail, favRecipeCreate, favMealTimeCreate, favMealDayCreate, favMealWeekCreate, favRecipeList, favMealTimeList, favMealDayList, favMealWeekList, ReviewDelete, FavMealTimeDelete, FavRecipeDelete, FavMealDayDelete, FavMealWeekDelete

urlpatterns = [
    path("signup", signup, name="signup"),
    path("login", auth_views.LoginView.as_view(), name="login"),
    path("logout", auth_views.LogoutView.as_view(), name="logout"),

    path("profiles/detail/", profileDetail, name="profile_detail"),

    path("reviews/<int:pk>/create", reviewCreate, name="review_create"),
    path("reviews/<int:pk>/update", reviewUpdate, name="review_update"),
    path("reviews/list", reviewList,name="review_list"),
    path("reviews/<int:pk>/delete",ReviewDelete.as_view(), name="review_delete"),

    path("<int:recipe_id>/shoppinglists/create", shoppingListCreate, name="shoppinglist_create"),
    path("shoppinglists/detail", shoppingListDetail, name="shoppinglist_detail"),

    path("favorites/recipes/<int:recipe_id>/create", favRecipeCreate,name="favrecipe_create"),
    path("favorites/recipes/<int:pk>/delete", FavRecipeDelete.as_view(), name="favrecipe_delete"),
    path("favorites/recipes/list", favRecipeList, name="favrecipe_list"),

    path("favorites/mealtimes/<int:mealtime_id>/create", favMealTimeCreate, name="favmealtime_create"),
    path("favorites/mealtimes/<int:pk>/delete", FavMealTimeDelete.as_view(), name="favmealtime_delete"),
    path("favorites/mealtimes/list", favMealTimeList, name="favmealtime_list"),

    path("favorites/mealdays/<int:mealday_id>/create", favMealDayCreate, name="favmealday_create"),
    path("favorites/mealdays/<int:pk>/delete", FavMealDayDelete.as_view(), name="favmealday_delete"),
    path("favorites/mealdays/list", favMealDayList, name="favmealday_list"),

    path("favorites/mealweeks/<int:mealweek_id>/create", favMealWeekCreate, name="favmealweek_create"),
    path("favorites/mealweeks/<int:pk>/delete", FavMealWeekDelete.as_view(), name="favmealweek_delete"),
    path("favorites/mealweeks/list", favMealWeekList, name="favmealweek_list"),
]

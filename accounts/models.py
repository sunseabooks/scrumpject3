from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings
USER_MODEL = settings.AUTH_USER_MODEL

# class FavUser(models.Model):
#     author = models.ForeignKey(
#         USER_MODEL, related_name="favusers_author", on_delete=models.CASCADE
#     )
#     friend = models.ForeignKey(
#         USER_MODEL, related_name="favusers_friends", on_delete=models.CASCADE
#     )

class FavRecipe(models.Model):
    name = models.CharField(max_length=35)
    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        USER_MODEL, related_name="favrecipes", on_delete=models.CASCADE
    )
    recipe = models.ForeignKey(
        "recipes.Recipe", related_name="favrecipes", on_delete=models.CASCADE
    )
    def __str__(self):
        return f"{self.recipe}"

class FavMealTime(models.Model):
    name = models.CharField(max_length=35)
    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        USER_MODEL, related_name="favmealtimes", on_delete=models.CASCADE
    )
    mealtime = models.ForeignKey(
        "mealplans.MealTime", related_name="favmealtimes", on_delete=models.CASCADE
    )
    def __str__(self):
        return f"{self.mealtime}"

class FavMealDay(models.Model):
    name = models.CharField(max_length=35)
    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        USER_MODEL, related_name="favmealdays", on_delete=models.CASCADE
    )
    mealday = models.ForeignKey(
        "mealplans.MealDay", related_name ="favmealdays", on_delete=models.CASCADE
    )
    def __str__(self):
        return f"{self.mealday}"

class FavMealWeek(models.Model):
    name = models.CharField(max_length=35)
    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        USER_MODEL, related_name="favmealweeks", on_delete=models.CASCADE
    )
    mealweek = models.ForeignKey(
        "mealplans.MealWeek", related_name="favmealweeks", on_delete=models.CASCADE
    )
    def __str__(self):
        return f"{self.author} "

class ShoppingList(models.Model):
    amount = models.CharField(max_length=100)
    author = models.ForeignKey(
        USER_MODEL, related_name="shoppinglists", on_delete=models.CASCADE
    )
    ingredient = models.ForeignKey(
        "recipes.Ingredient", related_name="shoppinglists", on_delete=models.CASCADE
    )
    def __str__(self):
        return f"{self.ingredient}"

class Review(models.Model):
    name = models.CharField(max_length=30)
    review = models.CharField(max_length=300)
    author = models.ForeignKey(
        USER_MODEL, related_name="reviews", on_delete=models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    rating = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    recipe = models.ForeignKey(
        "recipes.Recipe", related_name="reviews", on_delete=models.CASCADE, null=True, blank=True
    )
    mealtime = models.ForeignKey(
        "mealplans.MealTime", related_name="reviews", on_delete=models.CASCADE, null=True, blank=True
    )
    mealday = models.ForeignKey(
        "mealplans.MealDay", related_name="reviews", on_delete=models.CASCADE, null=True, blank=True
    )
    mealweek = models.ForeignKey(
        "mealplans.MealWeek", related_name="reviews", on_delete=models.CASCADE, null=True, blank=True
    )
    def __str__(self):
        return f"{self.author} on {self.created}"

from django.conf import settings
from django import forms
from .models import Review

# USER = settings.AUTH_USER_MODEL

# baseList = []

# def fieldsCompiler(request, baseList, extraDict):
#     fieldsList = baseList
#     for flag in extraDict:
#         if flag in request.path:
#             field = extraDict.get(flag)
#             fieldsList.append(field)
#     return fieldsList

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = [
            "name",
            "review",
            "rating",
        ]

from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from recipes.models import Ingredient
from .models import ShoppingList, Review, FavMealTime, FavMealDay, FavMealWeek, FavRecipe
from mealplans.models import MealTime, MealDay, MealWeek
from recipes.models import Recipe
from .forms import ReviewForm
from recipes.search_function import contentParse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import DeleteView


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password2"]
            user = User.objects.create_user(
                username=username, password=password
            )
            user.save()
            login(request, user)
            return redirect("profile_detail")
    else:
        form = UserCreationForm()
    context = {"form": form}
    return render(request, "registration/signup.html", context)

@login_required
def profileDetail(request):
    profileQuery = {
        "favmealtime": FavMealTime,
        "favmealday": FavMealDay,
        "favmealweek": FavMealWeek,
        "favrecipe": FavRecipe,
        "mealtime": MealTime,
        "mealday": MealDay,
        "mealweek": MealWeek,
        "recipe": Recipe,
    }

    dict = {}
    for variable in profileQuery:
        model = profileQuery.get(variable)
        try:
            dict[variable] = model.objects.filter(author=request.user).latest("created")
        except model.DoesNotExist:
            dict[variable] = ""

    context = {
        "favmealtime": dict.get("favmealtime"),
        "favmealday": dict.get("favmealday"),
        "favmealweek": dict.get("favmealweek"),
        "favrecipe": dict.get("favrecipe"),
        "mealtime": dict.get("mealtime"),
        "mealday": dict.get("mealday"),
        "mealweek": dict.get("mealweek"),
        "recipe": dict.get("recipe"),
    }
    return render(request, "profiles/detail.html", context)


@login_required
def reviewCreate(request, pk):
    review_param = {
        "mealtime":MealTime,
        "mealday":MealDay,
        "mealweek":MealWeek,
        "recipe":Recipe,
    }
    if request.method == "POST":
        form = ReviewForm(request.POST)
        print(form)
        if form.is_valid():
            reivew_items = dict(
                author=request.user,
                name=form.cleaned_data["name"],
                review=form.cleaned_data["review"],
                rating=form.cleaned_data["rating"],
                )
            for param in review_param:
                if param in request.GET.get("review"):
                    url = param
                    model = review_param.get(param)
                    review_ref = {
                        "mealtime": dict(mealtime=model.objects.get(pk=pk), **reivew_items),
                        "mealday": dict(mealday=model.objects.get(pk=pk), **reivew_items),
                        "mealweek": dict(mealweek=model.objects.get(pk=pk), **reivew_items),
                        "recipe": dict(recipe=model.objects.get(pk=pk), **reivew_items),
                    }
                    if param in review_ref:
                        review_items = review_ref.get(param)

            review = Review.objects.create(**review_items)
            review.save()
            return redirect(f"{url}_detail", pk)
    else:
        form = ReviewForm()
    context = {"form": form}
    return render(request, "reviews/create.html", context)

@login_required
def reviewUpdate(request, pk):
    old = Review.objects.get(pk=pk)
    if request.method == "POST":
        form = ReviewForm(request.POST, instance=old)
        if form.is_valid():
            review = form.save(commit=False)
            review.save()
            form.save_m2m()
        return redirect("review_list")
    else:
        form = ReviewForm(instance=old)
    context = {"form": form}
    return render(request, "reviews/update.html", context)

@login_required
def shoppingListDetail(request):
    if request.method == "POST":
        pk = request.POST.get("delete")
        shoppingdelete= ShoppingList.objects.get(pk=pk)
        shoppingdelete.delete()
        return redirect("shoppinglist_detail")
    else:
        shoppinglist = ShoppingList.objects.filter(author=request.user)
    context = {"shoppinglist": shoppinglist}
    return render(request, "shoppinglists/detail.html", context)

@login_required
def shoppingListCreate(request, recipe_id):
    if request.method == "POST":
        food_id = request.POST.get("shopping")
        ingredient = Ingredient.objects.get(pk=food_id)
        shop_amount = f"{ingredient.amount} {ingredient.measure}"
        shop_item = f"{ingredient.food}"
        try:
            item = ShoppingList.objects.get(ingredient=shop_item)
            item.amount = f"{item.amount} + {shop_amount}"
            item.save()
        except ShoppingList.DoesNotExist:
            ShoppingList.objects.create(
                amount=shop_amount,
                ingredient=shop_item,
                author=request.user)
        return redirect("recipe_detail", recipe_id)

@login_required
def favRecipeCreate(request, recipe_id):
    if request.method == "POST":
        recipe = Recipe.objects.get(pk=recipe_id)
        try:
            test = FavRecipe.objects.get(recipe=recipe)
            pass
        except FavRecipe.DoesNotExist:
            FavRecipe.objects.create(
                name=recipe.name,
                author=request.user,
                recipe=recipe
            )
        return redirect("recipe_detail", recipe_id)

@login_required
def favMealTimeCreate(request, mealtime_id):
    if request.method == "POST":
        mealtime = MealTime.objects.get(pk=mealtime_id)
        try:
            test = FavMealTime.objects.get(mealtime=mealtime)
            pass
        except FavMealTime.DoesNotExist:
            FavMealTime.objects.create(
                name=mealtime.name,
                author=request.user,
                mealtime=mealtime
            )
        return redirect("mealtime_detail", mealtime_id)

@login_required
def favMealDayCreate(request, mealday_id):
    if request.method == "POST":
        mealday = MealDay.objects.get(pk=mealday_id)
        try:
            test = FavMealDay.objects.get(mealday=mealday)
            pass
        except FavMealDay.DoesNotExist:
            FavMealDay.objects.create(
                name=mealday.name,
                author=request.user,
                mealday=mealday
            )
        return redirect("mealday_detail", mealday_id)

@login_required
def favMealWeekCreate(request, mealweek_id):
    if request.method == "POST":
        mealweek = MealWeek.objects.get(pk=mealweek_id)
        try:
            test = FavMealWeek.objects.get(mealweek=mealweek)
        except FavMealWeek.DoesNotExist:
            FavMealWeek.objects.create(
                name=mealweek.name,
                author=request.user,
                mealweek=mealweek
            )
        return redirect("mealweek_detail", mealweek_id)

@login_required
def reviewList(request):
    reviews = Review.objects.filter(author=request.user)
    context = {"reviews": reviews}
    return render(request, "review_list", context)

@login_required
def favRecipeList(request):
    favrecipes = FavRecipe.objects.filter(author=request.user)
    context = {"favrecipes": favrecipes}
    return render(request,"favorites/recipes/list.html", context)

@login_required
def favMealTimeList(request):
    favmealtimes = FavMealTime.objects.filter(author=request.user)
    context = {"favmealtimes": favmealtimes}
    return render(request, "favorites/mealtimes/list.html", context)

@login_required
def favMealDayList(request):
    favmealdays = FavMealDay.objects.filter(author=request.user)
    context= {"favmealdays": favmealdays}
    return render(request, "favorites/mealdays/list.html", context)

@login_required
def favMealWeekList(request):
    favmealweeks = FavMealWeek.objects.filter(author=request.user)
    context = {"favmealweeks": favmealweeks}
    return render(request, "favorites/mealweeks/list.html", context)

@login_required
def shoppingListDelete(request):
    model = ShoppingList
    options = {
        "ALL":model.objects.filter(author=request.user),
        "ONE":model.objects.get(pk=request.POST.get("ONE"))
    }
    item = [options.get(x) for x in options if x in request.POST]
    item[0].delete()
    return redirect("shoppinglist_detail")

class ReviewDelete(LoginRequiredMixin, DeleteView):
    model = Review
    template_name="reviews/delete.html"
    success_url=reverse_lazy("review_list")

class FavRecipeDelete(LoginRequiredMixin, DeleteView):
    model = FavRecipe
    template_name="favorites/recipes/delete.html"
    success_url=reverse_lazy("favrecipe_list")

class FavMealTimeDelete(LoginRequiredMixin, DeleteView):
    model = FavMealTime
    template_name="favorites/mealtimes/delete.html"
    success_url=reverse_lazy("favmealtime_list")

class FavMealDayDelete(LoginRequiredMixin, DeleteView):
    model = FavMealDay
    template_name="favorites/mealdays/delete.html"
    success_url=reverse_lazy("favmealday_list")

class FavMealWeekDelete(LoginRequiredMixin, DeleteView):
    model = FavMealWeek
    template_name="favorites/mealweeks/delete.html"
    success_url=reverse_lazy("favmealweek_list")
